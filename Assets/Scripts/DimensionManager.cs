﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Dimension
{
    WHITE,
    RED,
    BLUE
};

public class DimensionManager : MonoBehaviour {

    static Dictionary<Dimension, DimensionCamera> DimensionCameras;
    static DimensionManager _instance;
    public static DimensionManager Instance { get { if (_instance == null) { _instance = FindObjectOfType<DimensionManager>(); } return _instance; } }
    void Init()
    {
        DimensionCameras = new Dictionary<Dimension, DimensionCamera>();

    }

    public DimensionCamera GetCamera(Dimension dim)
    {
        if (DimensionCameras.ContainsKey(dim))
        {
            return DimensionCameras[dim];
        } else
        {
            return null;
        }
    }

    public void SetCamera(Dimension dim, DimensionCamera cam)
    {
        if(DimensionCameras == null)
        {
            DimensionCameras = new Dictionary<Dimension, DimensionCamera>();
        }
        DimensionCameras[dim] = cam;
    }

	// Use this for initialization
	void Start () {
            Init();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
