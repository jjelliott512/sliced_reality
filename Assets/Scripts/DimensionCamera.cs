﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DimensionCamera : MonoBehaviour {

    [SerializeField]
    public Dimension dim;
    Camera mCam;
	// Use this for initialization
	void Start () {
        mCam = GetComponent<Camera>();
        DimensionManager.Instance.SetCamera(dim, this);
	}

    public List<Vector2> GetUVForWorldVerts(List<Vector3> worldVerts)
    {
        List<Vector2> uvs = new List<Vector2>();
        foreach(var v in worldVerts)
        {
            uvs.Add(mCam.WorldToViewportPoint(v));
        }

        return uvs;
    }
}
