﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceScript : MonoBehaviour {

    [SerializeField]
    public Dimension dimension;

    DimensionCamera mDimCam;
    MeshFilter mFilter;

    private void Start()
    {
        mDimCam = DimensionManager.Instance.GetCamera(dimension);
        mFilter = GetComponent<MeshFilter>();
    }


    void Update () {

        List<Vector3> worldVerts = new List<Vector3>();

        foreach (var v in mFilter.mesh.vertices)
        {
            worldVerts.Add(transform.TransformPoint(v));
        }
        var newUVs = mDimCam.GetUVForWorldVerts(worldVerts);
        mFilter.mesh.SetUVs(0, newUVs);
        mFilter.mesh.RecalculateTangents();

	}
}
